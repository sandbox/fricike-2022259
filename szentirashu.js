/**
 * @file
 * All the JS side functions for the Szentiras.hu reftagger module.
 */
var hideit = true;

jQuery(document).ready(function(){
    jQuery('body').append(szentirashu_box);

    jQuery('.szentirashu_popup').each(function(){
      jQuery(this).mouseenter(
        function(e){
        jQuery('#szentirashu_wrapper').remove();
        xleft = 10 + e.pageX;
        xtop = e.pageY;
        jQuery('body').append(szentirashu_box);
        xverse = jQuery(this).attr('verse');
        //		    jQuery('#szentirashu_wrapper').css({position:"absolute", left:xleft, top:xtop}).delay(300).fadeIn('fast');
        jQuery('#szentirashu_wrapper').css({position:"absolute", left:xleft, top:xtop}).show();
        jQuery.ajax({
          url: szentirashu_proxy+xverse,
          type: 'GET',
          dataType: 'json',
          complete: function(msg){
          out = '';
          resp = jQuery.parseJSON(msg.responseText);
          if (resp['fromcache']) {
//          jQuery('#szentirashu_title').text(resp['hivatkozas']+' f('+resp['forditas']+') fv('+resp['forditas_valasz']+')');
          jQuery('#szentirashu_title').text(resp['hivatkozas']);
            out = resp['szoveg'];
            } else {
            jQuery('#szentirashu_title').text(resp['keres']['hivatkozas']);
            jQuery.each(resp['valasz']['versek'], function(k,v){
              out = out + v.szoveg + ' ';
              });
            }
            if (out.length > 300) {
            out = out.replace(/^(.{300}[^\s]*).*/, "$1") + " ...";
            }
            jQuery('#szentirashu_content').html(out);
            jQuery('#szentirashu_wrapper').css('height', 'auto');
            },
            });
        }).mouseleave(function(){
          if (hideit) {
          jQuery('#szentirashu_wrapper').remove();
          }
          })
    });

    jQuery('#szentirashu_wrapper')
      .mouseenter(function(e){
          hideit = false;
          })
    .mouseleave(function(){
        hideit = true;
        jQuery('#szentirashu_wrapper').remove();
        });
});






// var out = '';
// var szentirashu_box = '<div id="szentirashu_wrapper"><div id="szentirashu_title"></div><div id="szentirashu_content"><img src="'+szentirashu_path+'/szentirashu_loading.gif" /></div></div>';
// //var szentirashu_proxy = "http://szentiras.hu/API/?feladat=idezet&forditas=KNB&hivatkozas=";
// var szentirashu_proxy = "http://szentiras.hu/API/?feladat=idezet&hivatkozas=";

//	var x = jQuery('#content').html().replace(/(Ter|Kiv|Lev|Szám|MTörv|Józs|Bír|Rút|1Sám|2Sám|1Kir|2Kir|1Krón|2Krón|Ezdr|Neh|Tób|Judit|Eszt|Jób|Zsolt|Péld|Préd|Én|Bölcs|Sír|Iz|Jer|Siralm|Bár|Ez|Dán|Óz|Jo|Ám|Abd|Jón|Mik|Náh|Hab|Szof|Agg|Zak|Mal|1Makk|2Makk|Mt|Mk|Lk|Jn|Csel|Róm|1Kor|2Kor|Gal|Ef|Fil|Kol|1Tessz|2Tessz|1Tim|2Tim|Tit|Filem|Zsid|Jak|1Pét|2Pét|1Ján|2Ján|3Ján|Júdás|Jel) ?(\d+,[0-9.-]+)/g, '<a href="'+szentirashu_proxy+'$1$2" verse="$1$2" class="szentirashu_popup">$1 $2</a>');
//	jQuery('#content').replaceWith(x);

//Ter Kiv Lev Szám MTörv Józs Bír Rút 1Sám 2Sám 1Kir 2Kir 1Krón 2Krón Ezdr Neh Tób Judit Eszt Jób Zsolt Péld Préd Én Bölcs Sír Iz Jer Siralm Bár Ez Dán Óz Jo Ám Abd Jón Mik Náh Hab Szof Agg Zak Mal 1Makk 2Makk Mt Mk Lk Jn Csel Róm 1Kor 2Kor Gal Ef Fil Kol 1Tessz 2Tessz 1Tim 2Tim Tit Filem Zsid Jak 1Pét 2Pét 1Ján 2Ján 3Ján Júdás Jel

//    $json = file_get_contents('http://szentiras.hu/API/?feladat=idezet&hivatkozas=1Kor2,10-14');
//    $data = json_decode($json, TRUE);
//echo "<pre>"; print_r($filter); print_r($text); exit;
//
//    if($data['valasz'] == '') echo '<strong>Hiba történt:</strong> '.$data['hiba'];
//    else {
//	echo "<strong>1Kor2,10-14:</strong><br>\n";
//	foreach($data['valasz']['versek'] as $vers) {
//	    echo $vers['szoveg']." ";
//	}
//    }



// egyben Jn1,1 es Lk1,1
// kulon Jn 1,1
// ponttal Jn 1.1 meg a Jn1.1
// hosszan John 1,1 es John1,1
// 
// kiprobaljuk zarojelben [Jn1,1]
