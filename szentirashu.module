<?php
/**
 * @file
 * All the PHP side functions for the Szentiras.hu reftagger module.
 */
define('SZENTIRASHU', 'http://szentiras.hu/');
define('SZENTIRASHU_API', SZENTIRASHU . 'API/?feladat=idezet'); //&hivatkozas=');

/**
 * Implements hook_init().
 */
function szentirashu_init() {
  $szentirashu_box = '<div id="szentirashu_wrapper"><div id="szentirashu_title"></div>' .
    '<div id="szentirashu_content"><img src="' .
    base_path() . drupal_get_path('module', 'szentirashu') .
    '/szentirashu_loading.gif" /></div></div>';
  $vars = 'var szentirashu_box = \'' . $szentirashu_box . '\';
  var szentirashu_proxy = "' . base_path() . 'szentirashu_proxy/";';
  drupal_add_js($vars , array('type' => 'inline', 'scope' => 'header'));
  $x = current_path();
  if ( strpos($x, '/edit') !== FALSE || strpos($x, 'admin') !== FALSE ) {
    drupal_add_js(base_path() . drupal_get_path('module', 'szentirashu') . '/szentirashu.js', array('type' => 'file', 'scope' => 'footer'));
  }
}

/**
 * Implements hook_perm().
 */
function szentirashu_permission() {
  return array(
    'access szentirashu parser' => array(
      'title' => t('Access Szentiras.hu parser'),
      'description' => t('Allow users to access Szentiras.hu parser'),
    ),
  );

}

/**
 * Implements hook_menu().
 */
function szentirashu_menu() {
  $items['szentirashu_proxy/%'] = array(
    'title' => 'szentirashu proxy',
    'type' => MENU_CALLBACK,
    'access callback' => TRUE,
    'page callback' => '_szentirashu_proxy',
    'page arguments' => array(1, 2),
  );
  $items['admin/config/szentirashu'] = array(
    'title' => 'Szentiras.hu reftagger',
    'description' => 'Configuration for Szentiras.hu reftagger module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('szentirashu_config_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/szentirashu/reset'] = array(
    'title' => 'Reset Szentiras.hu database',
    'description' => 'Reset database for Szentiras.hu reftagger module',
    'page callback' => '_szentirashu_reset',
    'access arguments' => array('access administration pages'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_form().
 */
function szentirashu_config_form($form, &$form_state) {
  $form['from'] = array(
    '#type' => 'item', 
    '#title' => t('How to reset'),
    '#markup' => "If you want to or need to reset the cache database, " . l(t('click here'), 'admin/config/szentirashu/reset', array()) . " please.",
  );
  $form['szentirashu_translation'] = array(
    '#type' => 'select',
    '#title' => t('Default translation'),
    '#options' => array(
      'KNB' => 'KNB - Káldi-Neovulgáta', 
      'SZIT' => 'SZIT - Szent István Társulati Biblia', 
      'UF' => 'UF - Magyar Bibliatársulat újfordítású Bibliája', 
      //	    'KG' => 'KG - Károli Gáspár revideált fordítása',
    ),
    '#default_value' => variable_get('szentirashu_translation', 'KNB'),
    '#description' => t('Please select the translation to be used for reference.')
  );
  return system_settings_form($form);
}

/**
 * Custom function to fetch, parse and return szentiras.hu data.
 */
function _szentirashu_proxy($verse) {
  $translation = variable_get('szentirashu_translation', 'KNB');
  $van = db_select('szentirashu_references', 'sz')
    ->fields('sz', array('hivatkozas', 'szoveg', 'forditas', 'forditas_valasz'))
    ->condition('forditas', $translation, '=')
    ->condition('hivatkozas', $verse, '=')
    ->execute()
    ->fetchAssoc();
  if (!empty($van)) {
    $van['fromcache'] = 1;
    echo json_encode($van); exit;
  } 
  else {
    $json = file_get_contents(SZENTIRASHU_API . '&forditas=' . $translation . '&hivatkozas=' . $verse);
    $data = json_decode($json, TRUE);
    if ($data['valasz'] == '') { 
      echo json_encode(
        array(
          'keres' => $data['keres'],
          'valasz' => array(
            'versek' => array(0 => array(
              'szoveg' => '<strong>Hiba történt!</strong><br />' . $data['hiba'],
              'hivatkozas' => $data['keres']['hivatkozas'],
            )),
          ),
        )
      );
    } 
    else {
      foreach ($data['valasz']['versek'] as $vers) {
        $szoveg .= $vers['szoveg'] . " ";
      }
      $nid = db_insert('szentirashu_references') // Table name no longer needs {}
        ->fields(array(
          'hivatkozas' => $verse,
          'forditas' => $translation,
          'forditas_valasz' => $data['valasz']['forditas']['rov'],
          'szoveg' => $szoveg,
        ))
        ->execute();
      echo $json; exit;
    }
  }
  exit;
}

/**
 * Custom function to fetch, parse and return szentiras.hu data.
 */
function _szentirashu_reset() {
  $result = db_truncate('szentirashu_references')->execute();
  drupal_set_message(t('Szentiras.hu cache database has been truncated.'));
  drupal_goto('admin/config/szentirashu');
}

/**
 * Implements hook_filter_info().
 */
function szentirashu_filter_info() {
  $filters = array();
  $filters['szentirashu'] = array(
    'title' => t('Szentiras.hu reference parser'),
    'description' => t('Allows simple references to be parsed and display Bible texts.'),
    'process callback' => '_szentirashu_filter_process',
    'tips callback' => '_szentirashu_filter_tips',
    'default settings' => array(
      'szentirashu_translation' => 'KNB',
    ),
    'cache' => FALSE,
  );
  return $filters;
} 

/**
 * Implements hook_filter_FILTER_process().
 */
function _szentirashu_filter_process($text, $filter) { 
  if (!user_access('access szentirashu parser')) return $text;
  $text = preg_replace(

    '/(Ter|Kiv|Lev|Szám|MTörv|Józs|Bír|Rut|Rút|1Sám|2Sám|1Kir|2Kir|1Krón|2Krón|Ezd|Ezdr|Neh|Tób|Jud|Judit|Esz|Eszt|Jób|Zsolt|Péld|Préd|Én|Bölcs|Sír|Iz|Jer|Sir|Siral|Siralm|Bár|Ez|Dán|Oz|Óz|Jo|Ám|Abd|Jón|Mik|Náh|Hab|Szof|Agg|Ag|Zak|Mal|1Mak|1Makk|2Mak|2Makk|Mt|Mk|Lk|Jn|3Jn|Ap[cC]sel|Csel|Róm|1Kor|2Kor|Gal|Ef|Fil|Kol|1Tessz|2Tessz|1Tim|2Tim|Tit|Filem|Zsid|Jak|1Pét|1Pt|2Pét|2Pt|1Ján|1Jn|2Ján|2Jn|3Ján|3Jn|Júd|Júdás|Jel) ?(\d+,[0-9.-]+)/'

    , '<a href="' . SZENTIRASHU . '$1$2" verse="$1$2" class="szentirashu_popup">$1 $2</a>', $text);
  return $text;
}

/**
 * Implements hook_filter_FILTER_tips().
 */
function _szentirashu_filter_tips($filter, $format, $long = FALSE) { 
  return t('<em>[Jn1,1]</em> replaced with a popup and a link.');
}





//	var x = jQuery('#content').html().replace(/(Ter|Kiv|Lev|Szám|MTörv|Józs|Bír|Rút|1Sám|2Sám|1Kir|2Kir|1Krón|2Krón|Ezdr|Neh|Tób|Judit|Eszt|Jób|Zsolt|Péld|Préd|Én|Bölcs|Sír|Iz|Jer|Siralm|Bár|Ez|Dán|Óz|Jo|Ám|Abd|Jón|Mik|Náh|Hab|Szof|Agg|Zak|Mal|1Makk|2Makk|Mt|Mk|Lk|Jn|Csel|Róm|1Kor|2Kor|Gal|Ef|Fil|Kol|1Tessz|2Tessz|1Tim|2Tim|Tit|Filem|Zsid|Jak|1Pét|2Pét|1Ján|2Ján|3Ján|Júdás|Jel) ?(\d+,[0-9.-]+)/g, '<a href="'+szentirashu_proxy+'$1$2" verse="$1$2" class="szentirashu_popup">$1 $2</a>');
#	var szentirashu_proxy = "http://szentiras.hu/API/?feladat=idezet&forditas=KNB&hivatkozas=";
#
//Ter Kiv Lev Szám MTörv Józs Bír Rút 1Sám 2Sám 1Kir 2Kir 1Krón 2Krón Ezdr Neh Tób Judit Eszt Jób Zsolt Péld Préd Én Bölcs Sír Iz Jer Siralm Bár Ez Dán Óz Jo Ám Abd Jón Mik Náh Hab Szof Agg Zak Mal 1Makk 2Makk Mt Mk Lk Jn Csel Róm 1Kor 2Kor Gal Ef Fil Kol 1Tessz 2Tessz 1Tim 2Tim Tit Filem Zsid Jak 1Pét 2Pét 1Ján 2Ján 3Ján Júdás Jel
