-- SUMMARY --

The Szentiras.hu reftagger module enables a filter that parses the content, 
looking for Bible references. If matched, it turns it into a link to 
szentiras.hu, and creates a Javascript popup that displays the referenced
verse using the API at szentiras.hu.

For a full description of the module, visit the project page:
  http://drupal.org/project/szentirashu

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/szentirashu


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see https://drupal.org/node/895232 for further information.

* Enable filter for any/all input formats at admin/config/content/formats

* Grant permission to Authenticated and/or Anonymous users

* Use Bible references according to http://www.ujszov.hu/rovjegyz.htm


-- CONFIGURATION --

* Change the Bible version in Administration » Configuration and modules »
  Szentiras.hu reftagger

* Configure user permissions in Administration » People » Permissions:

  - Access Szentiras.hu parser

    Users in roles with the "Access administration menu" permission will see
    the administration menu at the top of each page.


-- TROUBLESHOOTING --

* If you see incorrect verse

  - Try to reset the cache database at admin/config/szentirashu/reset


-- TODOs --

* Put the popup is a alterable template somehow

* Don't close popup if mouse moves over the popup form the link.

* Regexp references according to the current translation's list


-- CONTACT --

Current maintainers:
* Frigyes Kovacs (fricike) - http://drupal.org/user/135707

This project has been sponsored by:
* Online Projects Kft
  By letting me work on this during work time. :)

* Szentiras.hu

